from app.AI import *
import os
import yaml
import socket
import select
import errno
from time import sleep


class Client:

    def __init__(self, sock=None):
        if (sock is None):
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = sock
        self.name = None
        self.actionCommand = None

    def connect(self):
        path = os.path.abspath(os.path.dirname(__file__))
        with open(path + "/config.yaml", 'r') as stream:
            params = yaml.load(stream)
        try:
            self.sock.connect((params['DEFAULT_IP'], params['DEFAULT_PORT']))
            self.getName()
            self.waitingBegin()
        except socket.error as e:
            if "Connection refused" in str(e):
                print("Erreur Connection to server {}".format(params['DEFAULT_IP']))
        if hasattr(self.sock, 'settimeout'):
            self.sock.settimeout(2000)
        print(params)

    def getName(self):
        data = self.sock.recv(20)
        self.name = data[4:-1]
        print(data)
        print("My name is {}".format(self.name))

    def waitingBegin(self):
        is_waiting = True
        while(is_waiting):
            data = self.sock.recv(20)
            if ("start" in str(data)):
                is_waiting = False
        self.launchGame()

    def launchGame(self):
        is_running = True
        # ai = ArtificialIntelligence(self.sock, self)
        # ai.start()
        while(is_running):
            try:
                ready_to_read, ready_to_write, in_error = select.select([self.sock, ], [self.sock, ], [], 4)
            except select.error:
                self.sock.shutdown(2)    # 0 = done receiving, 1 = done sending, 2 = both
                self.sock.close()
                print("connection error")
            if len(ready_to_read) > 0:
                data_received = self.sock.recv(10)
                print("received: {}".format(data_received))
            if len(ready_to_write) > 0:
                command = "left"
                self.sock.send(command.encode())
                sleep(0.05)
