from threading import Thread


class ArtificialIntelligence(Thread):

    def __init__(self, sock, client):
        Thread.__init__(self)
        self.sock = sock
        self.client = client

    def run(self):
        """Code à exécuter pendant l'exécution du thread."""
        command = "forward"
        while(True):
            self.sock.send(command.encode())
